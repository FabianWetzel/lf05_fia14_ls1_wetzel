
public class ZweiteKlasse {

	public static void main(String[] args) {

		//Aufgabe 1
		
		String stern = "*";
		
//		System.out.printf("%4s" + "%s\n" + "%s" + "%7s\n" + "%s" + "%7s\n" + "%4s" + "%s" , stern, stern, stern, stern, stern, stern, stern, stern);
		System.out.printf("Aufgabe 1:\n" + "%4s" + "%s\n" , stern, stern);
		System.out.printf("%s" + "%7s\n" , stern, stern); 
		System.out.printf("%s" + "%7s\n" , stern, stern);
		System.out.printf("%4s" + "%s\n\n" , stern, stern);

		//Aufgabe 2
		
		int zahl = 0;
		int ergebnis = 1;
		String gleich = "=";
		String hauptteil = "!   = " + (zahl + 1) + " * " + (zahl + 2) + " * " + (zahl + 3) + " * " + (zahl + 4) + " * " + (zahl + 5);
//		String ausgabe = zahl + "%s" + ergebnis + "\n";
		System.out.printf("Aufgabe 2:\n" + zahl + "%.5s" + " %19s" + "%4s" + "\n" , hauptteil, gleich, ergebnis);
		zahl = zahl + 1;
		System.out.printf(zahl + "%.8s" + " %16s" + "%4s" + "\n" , hauptteil, gleich ,ergebnis);
		ergebnis = 2;
		zahl = zahl + 1;
		System.out.printf(zahl + "%.12s" + " %12s" + "%4s" + "\n" , hauptteil, gleich ,ergebnis);
		ergebnis = 6;
		zahl = zahl + 1;
		System.out.printf(zahl + "%.16s" + " %8s" + "%4s" + "\n" , hauptteil, gleich ,ergebnis);
		ergebnis = 24;
		zahl = zahl + 1;
		System.out.printf(zahl + "%.20s" + " %4s" + "%4s" + "\n" , hauptteil, gleich ,ergebnis);
		ergebnis = 120;
		zahl = zahl + 1;
		System.out.printf(zahl + "%.28s" + " %s" + "%4s" + "\n" , hauptteil, gleich ,ergebnis);
		
		//Aufgabe 3
		
		System.out.println("\nAufgabe 3:" + "\nFahrenheit | Celsius");
		
		double f1 = -20;
		double f2 = -10;
		double f3 = 0;
		double f4 = 30;
		double f5 = 20;
		
		double c1= -28.8889;
		double c2= -23.3333; 
		double c3= -17.7778;
		double c4= -6.6667;
		double c5= -1.1111;
		String strich = " ";
		System.out.println(f1);
	}

}
