import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class Array�bungen {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		Random rand = new Random();
		// Aufgabe 1
		// zahlen();

		// Aufgabe 2
		// ungeradeZahlen();

		// Aufgabe 3
		// palindrom(myScanner);

		// Aufgabe 4
		// lotto(rand);
	}

	public static void zahlen() {
		System.out.println("Aufgabe 1: Liste");
		int[] zahlen = new int[10];
		for (int i = 0; i < 10; i++) {
			zahlen[i] = i;
		}
		for (int i = 0; i < 10; i++) {
			System.out.println(zahlen[i]);
		}
	}

	public static void ungeradeZahlen() {
		System.out.println("Aufgabe 2: Ungerade");
		int[] ungerade = new int[10];
		int zaehler = 0;
		int zahl = 0;
		while (zaehler < 10) {
			if (zahl % 2 == 1) {
				ungerade[zaehler] = zahl;
				zahl = zahl + 1;
				zaehler += 1;
			} else
				zahl = zahl + 1;
		}

		for (int i = 0; i < 10; i++) {
			System.out.println(ungerade[i]);
		}
	}

	public static void palindrom(Scanner myScanner) {
		System.out.println("Aufgabe 3: Palindrom");
		System.out.println("F�nf Zahlen eingeben:");
		int[] liste = new int[5];

		for (int i = 0; i < 5; i++) {
			liste[i] = myScanner.nextInt();
		}

		for (int i = 4; i >= 0; i--) {
			System.out.println(liste[i]);
		}

	}

	public static void lotto(Random rand) {
		int[] lotto = new int[6];
		boolean bereits = false;
		for (int i = 0; i < 6; i++) {
			int neu = rand.nextInt(48) + 1;
			for (int j = 0; j < i; j++) {
				if (lotto[j] == neu) {
					bereits = true;
				}
			}
			if (bereits == false) {
				lotto[i] = neu;
			} else {
				bereits = false;
				i = i - 1;
			}

		}
		Arrays.sort(lotto);
		System.out.println("Die Lottozahlen lauten:");
		System.out.print("[");
		for (int i = 0; i < 6; i++) {
			System.out.printf(" %s", lotto[i]);
		}
		System.out.print(" ] \n");

		for (int i = 0; i < 6; i++) {
			if (lotto[i] == 12) {
				System.out.println("Die 12 kommt in den Zahlen vor!");
			} else if (lotto[i] == 13) {
				System.out.println("Die 13 kommt in den Zahlen vor!");
			}
		}
	}
}