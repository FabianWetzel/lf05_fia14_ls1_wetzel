package Freie�bungen;
import java.util.Scanner;

public class Kiste {
	
	private int hoehe;
	private int breite;
	private int tiefe;
	private String farbe;
	

	public Kiste(int hoehe, int breite, int tiefe, String farbe) {
		this.hoehe = hoehe;
		this.breite = breite;
		this.tiefe = tiefe;
		this.farbe = farbe;
	}
	
	public int getHoehe() {
		return this.hoehe;
	}
	
	public int getBreite() {
		return this.breite;
	}
	
	public int getTiefe() {
		return this.tiefe;
	}
	
	public String getFarbe() {
		return this.farbe;
	}
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Eingabe: H�he, Breite, Tiefe, Farbe");
		int hoehe = tastatur.nextInt();
		int breite = tastatur.nextInt();
		int tiefe = tastatur.nextInt();
		String farbe = tastatur.next();
		
		Kiste kiste1 = new Kiste(hoehe, breite, tiefe, farbe);
		
		System.out.println("H�he = " + kiste1.getHoehe() + " / Breite = " + kiste1.getBreite() + " / Tiefe = " + kiste1.getTiefe() + " / Farbe = " + kiste1.getFarbe());
		
		tastatur.close();
	}
}
