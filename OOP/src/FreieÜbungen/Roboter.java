package Freie�bungen;

public class Roboter {

	public String name;
	public String farbe;
	public boolean ausgeschaltet;

	public Roboter() {
		this.name = "Robo Tobo";
		this.farbe = "t�rkis";
	}

	public Roboter(String name, String farbe) {
		this.name = name;
		this.farbe = farbe;
	}

	public void sprechen() {
		System.out.println("Hallo ich bin " + this.name + ", meine Farbe ist " + this.farbe);
	}

	public void rollen() {

	}

	public void nachricht(String a) {

	}

	public static void main(String[] args) {
		Roboter robo = new Roboter();
		robo.sprechen();
		robo.farbe = "blau";
		robo.sprechen();
	}

}
