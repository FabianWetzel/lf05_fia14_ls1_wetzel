package Freie�bungen;
public class SparbuchTest {
	public static void main(String[] args) {

		Sparbuch sb = new Sparbuch(1104711, 1000, 3);
		
		sb.getKontonummer();
		
		sb.zahleEin(60000);

		System.out.println("Kapital: " + sb.getKapital());
		System.out.println("Ertrag nach 6 Jahr: " + String.format("%.2f", sb.getErtrag(6)));

		sb.verzinse();
		System.out.println("Kapital: " + sb.getKapital());
	}
}
