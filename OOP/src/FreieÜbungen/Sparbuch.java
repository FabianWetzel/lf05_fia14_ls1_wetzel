package Freie�bungen;


public class Sparbuch {

	private int kontonummer;
	private double kapital;
	private double zinssatz;

	public Sparbuch(int kontonummer, double kapital, double zinssatz) {
		this.kontonummer = kontonummer;
		this.kapital = kapital;
		this.zinssatz = zinssatz;
	}

	public void zahleEin(double einzahlung) {
		this.kapital = this.kapital + einzahlung;
	}

	public void zahleAus(double auszahlung) {
		this.kapital = this.kapital - auszahlung;
	}

	public int getKontonummer() {
		return this.kontonummer;
	}

	public double getKapital() {
		return this.kapital;
	}

	public double zinssatz() {
		return this.zinssatz;
	}

	public double getErtrag(int jahre) {
		double kapital = this.kapital;
		for (int i = 0; i < jahre; i++) {
			kapital = kapital + (kapital /100 * this.zinssatz);
		}
		return kapital;
	}

	public void verzinse() {
		this.kapital = this.kapital + (this.kapital / 100 * this.zinssatz);
	}
}
