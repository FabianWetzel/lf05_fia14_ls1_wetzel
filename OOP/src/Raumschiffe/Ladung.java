package Raumschiffe;

public class Ladung {
	private String bezeichnung;
	private int groesse;
	
	public Ladung(String bezeichnung, int groesse) {
		this.bezeichnung = bezeichnung;
		this.groesse = groesse;
	}
	
	public String getBezeichnung(){
		return this.bezeichnung;
	}
	
	public void setBezeichnung(String neueBezeichnung) {
		this.bezeichnung = neueBezeichnung;
	}
	
	public int getGroesse() {
		return this.groesse;
	}
	
	public void setGroesse(int neueMenge) {
		this.groesse = neueMenge;
	}
	
}
