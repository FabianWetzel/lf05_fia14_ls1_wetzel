package Raumschiffe;

public class Raumschiff {
	private String schiffsname;
	private int energieversorgung;
	private int schutzschild;
	private int huelle;
	private int lebenserhaltung;
	private Ladung[] waffen;
	private int reparaturAndroiden;
	private int kapazitšt;
	
//	private String ladungsverzeichnis;
	private String broadcast;
	
	public Raumschiff(String name, int energieversorung, int schutzschild, int huelle, int lebenserhaltung, int reparaturAndroiden, int kapazitšt) {
		this.schiffsname = name;
		this.energieversorgung = energieversorung;
		this.schutzschild = schutzschild;
		this.huelle = huelle;
		this.lebenserhaltung = lebenserhaltung;
		this.reparaturAndroiden = reparaturAndroiden;
		this.kapazitšt = kapazitšt;
		this.waffen = new Ladung[kapazitšt];
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public String getBroadcast() {
		return this.broadcast;
	}
	
	public void setBroadcast(String broadcast) {
		this.broadcast = broadcast;
	}
	
	public int getEnergieversorgung() {
		return this.energieversorgung;
	}
	
	public void setEnergieversorgung(int neueEnergie) {
		this.energieversorgung = neueEnergie;
	}
	
	public void changeEnergieversorgung(int aenderungsEnergie) {
		this.energieversorgung = this.energieversorgung + aenderungsEnergie;
	}
	
	public int getSchutzschild() {
		return this.schutzschild;
	}
	
	public void SetSchutzschild(int neuesSchild) {
		this.schutzschild = neuesSchild;
	}
	
	public void changeSchutzschild(int aenderungsSchild) {
		this.schutzschild = this.schutzschild + aenderungsSchild;
	}
	
	public int getHuelle() {
		return this.huelle;
	}
	
	public void setHuelle(int neueHuelle) {
		this.huelle = neueHuelle;
	}
	
	public void changeHuelle(int aenderungsHuelle) {
		this.huelle = this.huelle + aenderungsHuelle;
	}
	
	public int getLebenserhaltung() {
		return this.lebenserhaltung;
	}
	
	public void setLebenserhaltung(int neueLebenserhaltung) {
		this.lebenserhaltung = neueLebenserhaltung;
	}
	
	public void changeLebenserhaltung(int aenderungsLebenserhaltung) {
		this.lebenserhaltung = this.lebenserhaltung + aenderungsLebenserhaltung;
	}
	
	public int getReparaturAndroiden() {
		return this.reparaturAndroiden;
	}
	
	public void setReparaturAndroiden(int neueReparaturAndroiden) {
		this.reparaturAndroiden = neueReparaturAndroiden;
	}
	
	public void changeReparaturAndroiden(int aenderungsReparaturAndroiden) {
		this.reparaturAndroiden = this.reparaturAndroiden + aenderungsReparaturAndroiden;
	}
	
	// Methoden
	
	public void schiessen(Ladung waffe, String ziel) {
		
	}
	
	public void addLadung(Ladung neueLadung) {
		for (int i = 0; i < kapazitšt; i++) {
			if (waffen[i] != null) {
				waffen[i] = neueLadung;
			}
		}
	}
	
	public void zeigeLadung() {
		for (int i = 0; i < kapazitšt; i++) {
			System.out.println(waffen[i].getBezeichnung());
		}
	}
	
}
