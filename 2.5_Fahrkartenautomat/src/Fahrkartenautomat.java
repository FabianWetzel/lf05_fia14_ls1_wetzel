import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		double zuZahlenderBetrag;
		int anzahl;
		System.out.print("Ticketpreis (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();

		System.out.print("Anzahl der Tickets: ");
		anzahl = tastatur.nextInt();
		zuZahlenderBetrag = zuZahlenderBetrag * anzahl;
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		double zwischensumme;
		double eingeworfeneM�nze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			zwischensumme = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zwischensumme);
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;

		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warte(250);
		System.out.println("\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag;
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {

			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, "2 EURO", 2.0);
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, "1 EURO", 1.0);
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, "50 CENT", 0.50);
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, "20 CENT", 0.20);

			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, "10 CENT", 0.10);
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, "5 CENT", 0.05);
				/*
				 * System.out.println("5 CENT"); r�ckgabebetrag -= 0.05; r�ckgabebetrag =
				 * Math.round(r�ckgabebetrag * 100) / 100.0;
				 */
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static void warte(int millisekunde) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static double muenzeAusgeben(double betrag, String einheit, double minus) {
		System.out.println(einheit);
		betrag -= minus;
		betrag = Math.round(betrag * 100) / 100.0;
		return betrag;
	}
}