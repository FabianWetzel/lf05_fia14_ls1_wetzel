import java.util.Scanner; // Import der Klasse Scanner 

public class Konsoleneingabe {

	public static void main(String[] args) // Hier startet das Programm
	{
		Scanner myScanner = new Scanner(System.in);

		String name;
		int alter;

		System.out.println("Willkommen zur Konsoleneingabe!\nWie hei�en Sie?");
		name = myScanner.nextLine();

		System.out.println("Wie alt sind Sie?");
		alter = myScanner.nextInt();

		System.out.println("Soso, Sie sind also " + name + " und dazu auch noch " + alter + " Jahre alt...");

		myScanner.close();
	}
}