
public class ErsteKlasse {

	public static void main(String[] args) {
		int alter = 28;
		String name = "Fabian";
		
		double eins = 22.4234234;
		float zwei = 5.95557773333333f;
		double drei = 4.0;
		double vier = 1000000.551;
		double fuenf = 97.34;
		
		System.out.print("Nun mit Variablen!\n" + "Hallo Welt! Mein Name ist " + name + ".\n");
		System.out.print("Ich bin " + alter + " Jahre alt. Ich begr��e Sie!\n\n");								//Formatierung mit Backslashes und Pluszeichen
		
		System.out.printf("Hier folgt eine %s Verarbeitung.", "String");
		String s = "***********";
		System.out.printf("\n%7.1s", s);
		System.out.printf("\n%8.3s", s);
		System.out.printf("\n%9.5s", s);
		System.out.printf("\n%10.7s", s);
		System.out.printf("\n%11.9s", s);
		System.out.printf("\n%12s", s);
		System.out.printf("\n%8.3s", s);
		System.out.printf("\n%8.3s", s);

		System.out.printf("\n\nHier folgt eine %s Verarbeitung.\n", "Integer");
		

		System.out.printf( "|%010.2f|\n" ,     eins);
		System.out.printf( "|%010.2f|\n" ,     zwei);
		System.out.printf( "|%010.2f|\n" ,     drei);
		System.out.printf( "|%010.2f|\n" ,     vier);
		System.out.printf( "|%010.2f|\n" ,     fuenf);
	}

}
