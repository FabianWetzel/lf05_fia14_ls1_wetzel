import java.util.Scanner;

public class Methoden {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		int anzahl = liesInt("", myScanner);
		String artikel = liesString("", myScanner);

		double nettopreis = liesDouble("", myScanner);
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double mwst = mehrwertsteuer(myScanner);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

	public static String liesString(String text, Scanner myScanner) {
		// Benutzereingaben lesen

		System.out.println("Was m�chten Sie bestellen?");
		String artikel = myScanner.next();
		// myScanner.close();
		return artikel;

	}

	public static int liesInt(String text, Scanner myScanner) {
		// Benutzereingaben lesen
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		// myScanner2.close();
		return anzahl;
	}

	public static double liesDouble(String text, Scanner myScanner) {
		// Benutzereingaben lesen
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		return preis;
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}

	public static double mehrwertsteuer(Scanner myScanner) {
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		return mwst;
	}

	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {

	}

}