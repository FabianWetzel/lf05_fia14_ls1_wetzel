
public class Konfigurationstest {

	public static void main(String[] args) {
		// Datentypen
		// �bung 1
		int cent;
		cent = 70;
		cent = 80;

		double maximum;
		maximum = 95.5;

		// �bung 2
		boolean status = true;
		short gewicht = -1000;
		float flaeche = 1.255f;
		char zeichen = '#';

		// �bung 3
		String satz = "Du bist toll!";
		final short CHECK_NR = 8765;

		// Direkte Anweisung an den Compiler, womit er es zu tun hat
		// Inhalt kann in entsprechend gro�e Container verstaut werden
		// Somit generelle Speichersparsamkeit
		// Weiterhin �bersichtlichkeit f�r den Programmierer

		// Operatoren

		// �bung 1

		int ergebnis = 4 + 8 * 9 - 1;
		System.out.println("Ergebnis: " + ergebnis);

		int zaehler = 1;
		zaehler++;
		System.out.println("Inkrement: " + zaehler);

		int division = 22 / 6;
		System.out.println("Division: " + division);
		
		//�bung 2
		int schalter = 10;
		boolean vergleich = schalter > 7 & schalter < 12;
		System.out.println("Schalter: " + vergleich);
		vergleich = schalter != 10 || schalter == 12;
		System.out.println("Schalter 2: " + vergleich);
		
		//�bung 3
		String oma = "Meine Oma " + "f�hrt im " + "H�hnerstall Motorrad.";
		System.out.println(oma + "\n\n");
		
	}

}
