import java.util.Random;
import java.util.Scanner;

public class ArrayFunktionen {

	public static void main(String[] args) {
		Random rand = new Random();
		Scanner myScanner = new Scanner(System.in);
		int[] basis = new int[10];

		for (int i = 0; i < 10; i++) {
			basis[i] = rand.nextInt(10);
		}
		// Aufgabe 1
		// System.out.println(arrayString(basis));

		// Aufgabe 2
		// umdrehen2(basis);

		// Aufgabe 3
		/*
		 * int[] gedreht = umdrehen2(basis); // �berpr�fung
		 * System.out.println("Gedrehtes Feld:"); for (int i = 0; i < 10; i++) { if (i <
		 * 9) { System.out.print(gedreht[i] + " - "); } else
		 * System.out.print(gedreht[i]); }
		 */

		// Aufgabe 4
		//System.out.println("Bitte Anzahl eingeben:");
		//int anzahl = myScanner.nextInt();
		//temperaturen(anzahl);
		
		// Aufgabe 5 und 6
		/*System.out.println("Gib erst 'n', dann 'm' ein.");
		int n = myScanner.nextInt();
		int m = myScanner.nextInt();
		int[][] matrizen = matrizen(n, m, myScanner);
		matrizenAusgabe(matrizen, n, m);*/
		
		//Aufgabe 6
		/*if (n == m) {
			transponiert(matrizen, n);
		}*/
	}

	public static String arrayString(int[] zahlen) {
		String erg = "";
		for (int i = 0; i < 10; i++) {
			erg = erg + zahlen[i] + " ";
		}
		return erg;
	}

	public static void umdrehen(int[] zahlen) {
		// Ausgabe Grundfeld
		System.out.println("Erstelltes Feld:");
		for (int i = 0; i < 10; i++) {
			if (i < 9) {
				System.out.print(zahlen[i] + " - ");
			} else
				System.out.print(zahlen[i]);
		}
		// Umdrehen
		int hoch = zahlen.length - 1;
		int halterHoch = 0;
		int halterTief = 0;
		for (int i = 0; i < zahlen.length / 2; i++) {
			halterTief = zahlen[i];
			halterHoch = zahlen[hoch];
			zahlen[i] = halterHoch;
			zahlen[hoch] = halterTief;
			hoch--;
		}
		// �berpr�fung
		System.out.println("Gedrehtes Feld:");
		for (int i = 0; i < 10; i++) {
			if (i < 9) {
				System.out.print(zahlen[i] + " - ");
			} else
				System.out.print(zahlen[i]);
		}
	}

	public static int[] umdrehen2(int[] zahlen) {
		// Ausgabe Grundfeld
		System.out.println("Erstelltes Feld:");
		for (int i = 0; i < 10; i++) {
			if (i < 9) {
				System.out.print(zahlen[i] + " - ");
			} else
				System.out.print(zahlen[i]);
		}
		// Umdrehen
		int[] gedreht = new int[10];

		int hoch = zahlen.length - 1;
		for (int i = 0; i < 10; i++) {
			gedreht[i] = zahlen[hoch];
			hoch--;
		}
		return gedreht;
	}

	public static void temperaturen(int anzahl) {
		// 1. Wert Fahrenheit, 2. Wert Grad Celsius (5/9) � (F � 32)
		double[][] tempe = new double[anzahl][2];
		double fahrenheit = 0;
		double celsius = 0;
		for (int i = 0; i < anzahl; i++) {
			fahrenheit = i * 10;
			celsius = (fahrenheit - 32) * 5 / 9;
			tempe[i][0] = fahrenheit;
			tempe[i][1] = celsius;
		}

		for (int i = 0; i < anzahl; i++) {
			System.out.printf("\n[ %.1f | %.1f ] ", tempe[i][0], tempe[i][1]);
		}
		
	}

	public static int[][] matrizen(int n, int m, Scanner myScanner) {
		int[][] matrizen = new int[n][m];
		System.out.println("Bitte die Werte eingeben. Erst die X-Achse, dann die Y-Achse der Matrix:");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				matrizen[i][j] = myScanner.nextInt();
			}
		}
		return matrizen;
	}
	
	public static void matrizenAusgabe(int[][] matrizen, int n, int m) {
		System.out.println("Ausgabe:");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.print("| " + matrizen[i][j] + " |");
			}
			System.out.println("");
		}
	}

	public static void transponiert(int[][] matrizen, int n) {
		boolean trans = true;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (matrizen[i][j] != matrizen[j][i]) {
					trans = false;
				}
			}
		}
		if (trans == true) {
			System.out.println("Die Matrix ist transponiert!");
		}
		else
			System.out.println("Die Matrix ist NICHT transponiert!");
	}

}
