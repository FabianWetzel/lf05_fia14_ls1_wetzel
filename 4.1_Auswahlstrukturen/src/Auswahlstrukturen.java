import java.util.Scanner;

public class Auswahlstrukturen {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		einsEins(myScanner);

		myScanner.close();
	}

	public static void einsEins(Scanner myScanner) {
		System.out.println("Bitte zwei Zahlen eingeben.");
		int erste = myScanner.nextInt();
		int zweite = myScanner.nextInt();
		if (erste == zweite) {
			System.out.println("Gleich.");
		}
		else if (zweite > erste) {
			System.out.println("Zweite gr��er.");
		}
		else if (erste > zweite) {
			System.out.println("Erste gr��er.");
		}
		
		System.out.println("Bitte drei Zahlen eingeben.");
		int eins = myScanner.nextInt();
		int zwei = myScanner.nextInt();
		int drei = myScanner.nextInt();
		
		if (eins > zwei && eins > drei) {
			System.out.println("Erste gr��te.");
		}
		
		if (drei > zwei || drei > eins) {
			System.out.println("Dritte ist nicht die kleinste");
		}
		
		if (eins > zwei && eins > drei) {
			System.out.println("Eins ist die gr��te.");
		}
		else if (zwei > eins && zwei > drei) {
			System.out.println("Zwei ist die gr��te.");
		}
		else if (drei > zwei && drei > eins) {
			System.out.println("Drei ist die gr��te.");
		}
		
	}
}