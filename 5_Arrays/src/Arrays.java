import java.util.Scanner;



public class Arrays {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int[] liste = eingabe(myScanner);
		mittelwert(liste);
	}

	public static int[] eingabe(Scanner myScanner) {

		System.out.println("Willkommen!");
		System.out.println("Wie viele Stellen soll die Liste haben?");
		int anzahl = myScanner.nextInt();
		System.out.println("Gib hintereinander die Zahlen an:");
		int arr[] = new int[anzahl];
		for (int i = 0; i < anzahl; i++) {
			arr[i] = myScanner.nextInt();
		}
		return arr;
	}

	public static void mittelwert(int[] liste) {
		int summe = 0;
		int anzahl = liste.length;
		for (int i = 0; i < anzahl; i++) {
			summe = summe + liste[i];
		}
		double mitte = summe / anzahl;
		ausgabe(mitte);
	}

	public static void ausgabe(double mittelwert) {
		System.out.println("Der Mittelwert lautet: " + mittelwert);
	}
}
