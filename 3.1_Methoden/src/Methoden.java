
public class Methoden {

	public static void main(String[] args) {
		aufgabe_multiplikation();
		aufgabe1();
	}

	static void aufgabe1() {
	      // (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
	      double x = 2.0;
	      double y = 4.0;
	      double m;
	      m = mittelwert(x, y);
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	}
	
	static double mittelwert(double eins, double zwei) {
		double erg;
		erg = (eins + zwei) / 2.0;
		return erg;
	}
	
	static void aufgabe_multiplikation() { // 1. Arbeitsblatt
		double eins = 2.36;
		double zwei = 7.87;
		double ergebnis = multiplikation(eins, zwei);
		System.out.println("Aufgabe 1: " + ergebnis);
	}
	
	static double multiplikation(double zahl1, double zahl2) {
		double erg = zahl1 * zahl2;
		return erg;
	}

}
